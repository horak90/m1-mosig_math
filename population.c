#include <stdlib.h>
#include <stdio.h>


double fRand(float fMin, float fMax)
{
    float f = (float)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}



float prob(float a, float b, int nbImm)
{	
	int i;
	float probA;
	float probB; 
	
	for(i = 0;i < nbImm; i++)
	{		
		float x = fRand(0.0,1.0);
		probA = a/(a+b);
		probB = b/(a+b);

		//printf("%f\n",x);

		if(x < probA)
		{
			a+=1;
		}else
		{
			b+=1;
		}
	}

	/*
	printf("Population A %f\n",a);
	printf("Population B %f\n",b);
	printf("Probability of immigration city A %f\n", probA);
	*/

	return probA;
}


int main (int argc, char *argv[])
{

	srand(time(NULL));

	float a;
	float b;
	int nbImm, i, y, rep = 10000;
	float med;
	FILE *fp;


	fp=fopen("test.txt", "a+");

	if(argc < 4)
	{
		printf("Missing arguments \n");
	}else
	{


		nbImm = atoi(argv[1]);
		a = atof(argv[2]);
		b = atof(argv[3]);
		int N = atoi(argv[4]);
		
		for (y = 0; y < rep; y++)
		{
			float arr[N];

			for(i = 0; i < N; i++)
			{
				arr[i] = prob(a,b,nbImm);
				//printf("Probability of immigration city A %f\n", arr[i]);
			}		

			med += arr[N-1];

			//printf("Probability of immigration city A %f\n", arr[N-1]);
			if(fp == NULL)
			{
				printf("FAIL\n");
				return;
			}   				



			fprintf(fp,"%f \n",arr[N-1]);
		}

		printf("Avarage val %f\n", med/rep);
		fclose(fp);		
	}

}