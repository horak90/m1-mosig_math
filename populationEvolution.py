import argparse
import random
import matplotlib.pyplot as plt



def prob(A,B,nbImm):	
	a = []
	b = []

	for i in range(nbImm):
		x = float(random.random())
		if float(x) < (A/float(A+B)):
			A+=1
			a.append(round(A/float(A+B),3))
		else:
			B+=1
			b.append(round(B/float(A+B),3))
	print("Population A =", A)
	print("Population B =", B)
	print("Probability of going to A =", (A/float(A+B)))
	return a, b

def main():

	parser = argparse.ArgumentParser()
	parser.add_argument("nbImm", type=int, help='Number of immigrants')
	parser.add_argument("initA", type=int, help='Initial population of A')
	parser.add_argument("initB", type=int, help='Initial population of B')	
	parser.add_argument("nbExp", type=int, help='Number of experiments to run')
	args = parser.parse_args()
 
	nbImm = args.nbImm
	N = nbImm - 1
	A = args.initA
	B = args.initB
	Ne = args.nbExp

	a = []
	b = []
	m = []

	for x in range(Ne):
		print("---------------------------------")
		print("Initial Population of A",A)
		print("Initial Population of B",B)
		print("Number of imigrants",nbImm)
		a, b = prob(A,B,nbImm)
		if(a):
			print("Limits of probability for each experiment ", max(a))
		if(a):
			m.append(max(a))		
		print("---------------------------------")



	plt.hist(m,30)

	#plt.hist(m, bins=10, histtype='stepfilled', normed=True, color='b', label='city A')
	plt.title("Imigration problem")
	plt.xlabel("Limit values of probabilities")
	plt.ylabel("Frequency")

	a, b = prob(A,B,nbImm)

	plt.legend()
	plt.show()


if __name__ == "__main__":
	main()






